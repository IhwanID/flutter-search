import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var searchController = TextEditingController();
  String hello = "test" ;
  List<String> listFood = [];
  List<String> searchResult = [];
  bool isSearch = false;

  @override
  void initState() {
    super.initState();
    listFood = ['nasi putih', 'sayur bayam', 'ayam goreng', 'ayam sayur', 'nasi uduk', 'nasi kuning'];
  }

  Widget buildAppBar(){
    if(isSearch){
      return AppBar(
        centerTitle: true,
        title: Padding(
          padding: EdgeInsets.only(top: 7),
          child: _formWhiteNoIcon(
            hint: "Search...",
            controller: searchController,
          ),
        ),
      );
    }else{
      return AppBar(
        centerTitle: true,
        title: Text('Search App'),
        actions: [IconButton(icon: Icon(Icons.search), onPressed: (){
         setState(() {
           isSearch = true;
         });
        })],
      );
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body: !isSearch ? buildListFood() : buildListResult(searchResult)
    );
  }

  Widget buildListFood(){
    return ListView.builder(
        itemCount: listFood.length,
        itemBuilder: (context, index){
          return ListTile(title: Text('${listFood[index]}'),);
        });
  }

  Widget buildListResult(List<String> result){
    if(result.length == 0) {
      return Text('No Food Found');
    }else{
      return ListView.builder(
          itemCount: searchResult.length,
          itemBuilder: (context, index){
            return ListTile(title: Text('${searchResult[index]}'),);
          });}
    }


  Widget _formWhiteNoIcon({String hint, TextEditingController controller}) {
    return Container(
        alignment: Alignment.centerLeft,
        decoration:
        BoxDecoration(image: DecorationImage(image: AssetImage(""))),
        margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
        child: Padding(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
          child: TextField(
            textAlign: TextAlign.justify,
            autofocus: true,
            style: TextStyle(color: Colors.white),
            onChanged: onSearchTextChanged,
            controller: controller,
            textInputAction: TextInputAction.search,
            decoration: InputDecoration(
              suffixIcon: GestureDetector(
                onTap: () {
                  print("input : " + controller.text);
                  onSearchTextChanged(controller.text);
                  FocusScope.of(context).requestFocus(FocusNode());
                },
                child: Icon(
                  Icons.search,
                  color: Colors.white,
                ),
              ),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(color: Colors.white)),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(color: Colors.white)),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(color: Colors.white)),
              contentPadding: EdgeInsets.fromLTRB(10, 8, 10, 13),
              hintText: hint,
              hintStyle: TextStyle(color: Colors.white),
            ),
          ),
        ));
  }

  void onSearchTextChanged(String value) {
    print("value : $value");
    searchResult = [];
    if(value.isEmpty || value == ""){
      setState(() {
        isSearch = false;
      });
    }else{
      listFood.forEach((f){
        if(f.contains(value)){
          searchResult.add(f);

        }
      });
    }
    print(searchResult);
  }
}



